$(function(){


//Todo App Model
var Todo = Backbone.Model.extend({
  defaults: function(){
    return{
      title: "Empty..",
      done: false
    };
  },
  initialize: function(){
    if(!this.get('title')){
        this.set({'title': this.defaults().title});
    }
  },

  toggle: function(){
    this.save({done: !this.get('done')});
  }
});

var TodoCollection = Backbone.Collection.extend({
  model: Todo,
  localStorage: new Backbone.LocalStorage('localstore'),

//Filter completed todos
  done: function(){
    //return this.filter(function(todo){return todo.get('done');});
    return this.where({done: true});

  },

  remaining: function(){
    //return this.without.apply(this,this.done());
    return this.where({done: false});
  },

});

var Todos = new TodoCollection;

// Backbone View for Todo List Template - Rendering Lists of Todos
var TodoItemView = Backbone.View.extend({
  tagName: 'li',
  template: _.template($('#item-template').html()),
  events : {
    "click .toggle": "toggleDone",
    "dblclick .view" : "edit",
    "click a.destroy" : "clearItem",
    "keypress .edit" : "updateItem",
    "blur .edit" : "closeEdit"

  },

//TodoItemView listens to changes in the TodoApp Model and
//updates the view of the template
  initialize: function(){
    this.listenTo(this.model,'change',this.render);
    this.listenTo(this.model,'destroy',this.remove);
    this.listenTo(this.model, 'add', this.toggleDone);

  },

  render: function(){
    this.$el.html(this.template(this.model.toJSON()));
    this.$el.toggleClass('done',this.model.get('done'));
    this.input = this.$('.edit');
    return this;
  },

  toggleDone: function(){
    this.model.toggle();
    var filterTodo = $('.filterTodo:checked').val();

    TodoStats.addAll(filterTodo);
  },

  edit: function(){
    this.$el.addClass('editing');
    this.input.focus();
  },

  updateItem: function(e){
    if (e.keyCode == 13) this.closeEdit();
  },

  closeEdit: function(){
      var value = this.input.val();
      if(!value){
        this.model.clear();
      } else {
        this.model.save({title: value});
        this.$el.removeClass('editing');
      }
  },
  clearItem: function(){
    this.model.destroy();
  }
});

// Template for Statistics for finished tasks at the bottom
//of the page
var TodoStatsView = Backbone.View.extend({
  //Element top-most div in the app #todoapp
  el : $('#todoapp'),
  //footer part of the app #stats-template
  statsTemplate: _.template($('#stats-template').html()),

  events :{
    "keypress #new-todo" : "addItem",
    "click #clear-completed" : "removeCompleted",
    "click #toggle-all" : "allCompleted",
    "click .filterTodo" : "filterTodoList"

  },

  initialize: function(){
    this.input = this.$('#new-todo');
    this.allCheckbox = this.$('#toggle-all')[0];
    this.main = this.$('#main');
    this.footer = this.$('footer');
    //this.notChecked = this.$('#not-yet-finished')[0];
    //this.allChecked = this.$('#all-finished')[0];

    this.listenTo(Todos, 'add', this.addOne);
    this.listenTo(Todos, 'reset', this.addAll);
    this.listenTo(Todos, 'all', this.render);

    Todos.fetch();

  },

  addOne: function(todo){
    var newItem = new TodoItemView({model: todo});
    this.$('#todo-list').append(newItem.render().el);
  },

  addAll: function(filterTodo){
    filterTodo = filterTodo !== undefined ? filterTodo : 'all';
    this.$('#todo-list').html('');
    switch (filterTodo) {
      case 'notyet':
        _.each(Todos.where({done: false}), this.addOne);
        break;
      case 'alldone':
        _.each(Todos.where({done: true}), this.addOne);
        break;
      default:
        Todos.each(this.addOne, this);
        break;
    }
  },

  render: function(){
    var done = Todos.done().length;
    var remaining = Todos.remaining().length;

    if(Todos.length){
        this.main.show();
        this.footer.show();
        this.footer.html(this.statsTemplate({done: done, remaining: remaining}));
    }
    else{
      this.main.hide();
      this.footer.hide();
    }

    this.allCheckbox.checked = !remaining;
  },

  filterTodoList: function(e) {
    var filter = $(e.currentTarget).val();
    alert(filter);
    var done = Todos.done().length;
    var remaining = Todos.remaining().length;
    $('#markall').show();
    switch (filter) {
      case 'notyet':
        alert('NotYet');
        this.addAll('notyet');
        $('#toggle-all').prop("checked", false);
        if(remaining==0){
          $('#markall').hide();
        }
        break;
      case 'alldone':
        this.addAll('alldone');
        $('#toggle-all').prop("checked", true);
        if(done==0){
            $('#markall').hide();
        }
        break;
      default:
        this.addAll('all');
        $('#toggle-all').prop("checked", false);
        if(done==Todos.length){
          $('#toggle-all').prop("checked", true);
        }
        break;
    }
  },

  addItem: function(e){
    if(e.keyCode != 13) return;
    if(!this.input.val()) return;

    Todos.create({title: this.input.val()});
    this.input.val('');
  },

  removeCompleted: function(){
    _.invoke(Todos.done(), 'destroy');
    return false;
  },

  allCompleted: function(){
     var tododone = this.allCheckbox.checked;
     var done = Todos.done().length;
     var remaining = Todos.remaining().length;

     Todos.each(function(todo){
       todo.save({'done': tododone});
     });
     var filterTodo = $('.filterTodo:checked').val();
     TodoStats.addAll(filterTodo);
     $('#markall').hide();
     switch (filterTodo) {
       case 'notyet':
         this.addAll('notyet');
         console.log(done + "," + remaining);
         if(remaining>0){
            $('#markall').show();
         }
         break;
       case 'alldone':
         this.addAll('alldone');
         if(done==0){
             $('#markall').hide();
         }
         break;
       default:
         this.addAll('all');
         $('#markall').show();
         /*if(done==Todos.length){
           $('#toggle-all').prop("checked", true);
         }*/
         break;
     }
  },

});

var TodoStats = new TodoStatsView;
});
